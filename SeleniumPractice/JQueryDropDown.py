from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select
import time

def select_values(option_list, value): #method for selecting multiple choices from dropdown
    if not value[0] == 'all':
        for ele in dropdown_list:
            print(ele.text)
            for k in range(len(value)):
                if ele.text == value[k]:
                    ele.click()
                    break

    else:
        try:
            for ele in option_list:
                ele.click()
        except Exception as e:
            print(e)

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.jqueryscript.net/demo/Drop-Down-Combo-Tree/")
print("Title of this page is: "+driver.title)

driver.find_element(By.ID, 'justAnInputBox').click()
time.sleep(3)

dropdown_list = driver.find_elements(By.CSS_SELECTOR, 'span.comboTreeItemTitle')
values_list = ['choice 1', 'choice 2', 'choice 6 2 1'] #can select multiple choices
values_list1 = ['choice 3'] #can select single choice
values_list3 = ['all'] #will select all choices
select_values(dropdown_list, values_list3)

#will perform multi select but without for loop
#select_values(dropdown_list, 'choice 2')
#select_values(dropdown_list, 'choice 4')
#select_values(dropdown_list, 'choice 3')





