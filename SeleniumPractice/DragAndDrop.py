from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://jqueryui.com/resources/demos/droppable/default.html")
print("Title of this page is: "+driver.title)

'''Drag And Drop Example'''

source_ele = driver.find_element(By.ID, 'draggable')
target_ele = driver.find_element(By.ID, 'droppable')

act_chains = ActionChains(driver)
act_chains.drag_and_drop(source_ele, target_ele).perform()

time.sleep(3)
driver.quit()

'''Another way'''

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://jqueryui.com/resources/demos/droppable/default.html")
print("Title of this page is: "+driver.title)

source_ele = driver.find_element(By.ID, 'draggable')
target_ele = driver.find_element(By.ID, 'droppable')

act_chains = ActionChains(driver)

act_chains.click_and_hold(source_ele).move_to_element(target_ele).release().perform()

time.sleep(3)
driver.quit()

