from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.get("https://www.orangehrm.com/orangehrm-30-day-trial/")
print("Title of this page is: "+driver.title)

username_url = driver.find_element(By.ID, 'Form_submitForm_subdomain')
first_name = driver.find_element(By.ID, 'Form_submitForm_FirstName')
last_name = driver.find_element(By.ID, 'Form_submitForm_LastName')
business_email = driver.find_element(By.CSS_SELECTOR, '#Form_submitForm_Email')
feature_link = driver.find_element(By.LINK_TEXT, 'Features')
job_title = driver.find_element(By.NAME, 'JobTitle')
username_url.send_keys("Python practice")
first_name.send_keys("Python")
last_name.send_keys("practice")
business_email.send_keys("abcd@gmail.com")
job_title.send_keys("Practice")
feature_link.click()

time.sleep(4)
driver.quit()