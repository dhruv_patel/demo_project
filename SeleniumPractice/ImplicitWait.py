from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.implicitly_wait(10)
#we are waiting for 10 sec for all web elements to load (Also called dynamic wait or global wait)
#only applicable for web elements (not for title, url, etc)
#we can overrite implicit wait again and again

driver.maximize_window()
driver.get("https://app.hubspot.com/login")
print("Title of this page is: "+driver.title)
username_ele = driver.find_element(By.ID, 'username')
username_ele.send_keys("test@gmail.com")