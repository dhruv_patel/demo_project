from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.amazon.ca/")
print("Title of this page is: "+driver.title)

driver.find_element(By.LINK_TEXT, "Best Sellers").click()
driver.back()
time.sleep(3)
driver.forward()
time.sleep(3)
driver.refresh()
time.sleep(3)
print("URL of this page is: "+driver.current_url)

driver.quit()