from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path="C:\\Users\\dpatel\\Downloads\\chromedriver_win32//chromedriver.exe")
driver.implicitly_wait(5)
driver.maximize_window()
driver.get("https://www.google.ca/")
print("The title of this page is: "+driver.title)

driver.find_element(By.NAME, 'q').send_keys("naveen automationlabs")

searchOptions = driver.find_elements(By.CSS_SELECTOR, 'ul.erkvQe li span')
print(len(searchOptions))

for ele in searchOptions:
    print(ele.text)
    if ele.text == 'naveen automationlabs youtube':
        ele.click()
        break

time.sleep(5)
driver.quit()