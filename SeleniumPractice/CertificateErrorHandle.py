from selenium import webdriver
from selenium.webdriver import ActionChains, DesiredCapabilities
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

from webdriver_manager.firefox import GeckoDriverManager

'''For Chrome: Method 1'''
#options = webdriver.ChromeOptions()
#options.add_argument('--allow-running-insecure-content')
#options.add_argument('--ignore-certificate-errors')
#driver = webdriver.Chrome(ChromeDriverManager().install(), options = options)

'''Method 2'''
#desired_capabilities = DesiredCapabilities().CHROME.copy()
#desired_capabilities['acceptInsecureCerts'] = True
#driver = webdriver.Chrome(ChromeDriverManager().install(), desired_capabilities = desired_capabilities)

'''For Firefox'''
profile = webdriver.FirefoxProfile()
profile.accept_untrusted_certs = True
driver = webdriver.Firefox(executable_path = GeckoDriverManager().install(), firefox_profile=profile)

driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://expired.badssl.com/")
print("Title of this page is: "+driver.title)

print(driver.find_element(By.TAG_NAME, 'h1').text)