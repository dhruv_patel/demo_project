from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.orangehrm.com/orangehrm-30-day-trial/")
print("Title of this page is: "+driver.title)

ele_indus =driver.find_element(By.ID, 'Form_submitForm_Industry')
select_indus = Select(ele_indus)

select_indus.select_by_visible_text('Electronics')
time.sleep(2)
select_indus.select_by_index(4)
time.sleep(2)
select_indus.select_by_value('Finance/Banking/Insurance/Real Estate/Legal')
time.sleep(2)
#there are deselcet methods as well same as select

ele_country = driver.find_element(By.ID, 'Form_submitForm_Country')
select_country = Select(ele_country)

select_country.select_by_visible_text('India')
time.sleep(2)

#we can also create one Select method to perform select operation multiple times

def select_values(element, value):
    select = Select(element)
    select.select_by_visible_text(value)

ele_indus =driver.find_element(By.ID, 'Form_submitForm_Industry')
ele_country = driver.find_element(By.ID, 'Form_submitForm_Country')

select_values(ele_indus, 'Education')
time.sleep(2)
select_values(ele_country, 'Canada')
time.sleep(2)

#we can get all the values of the dropdown using options method in Select

select = Select(ele_indus)
indus_list = select.options

for ele in indus_list:
    print(ele.text)
    if(ele.text == "Automotive"): #we can choose particular value from dropdown
        ele.click()
        break
        time.sleep(3)

#select the dropdown without Select class

indus_options = driver.find_elements(By.XPATH, '//select[@id="Form_submitForm_Industry"]/option')
print(len(indus_options))
for ele in indus_options:
    print(ele.text)
    if ele.text == 'Travel':
        ele.click()
        break
        time.sleep(3)

driver.quit()

