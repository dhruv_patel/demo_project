from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

options = webdriver.ChromeOptions()
driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.amazon.ca/")
print("Title of this page is: "+driver.title)

best_sellers = driver.find_element(By.LINK_TEXT, 'Best Sellers')

#To perform click
'''driver.execute_script("arguments[0].click();", best_sellers)'''

#To get title
'''title = driver.execute_script("return document.title;")'''
'''print(title)'''

#To refresh the webpage
'''driver.execute_script("history.go(0);")'''

#To generate alert
'''driver.execute_script("alert('Hello Dhruv');")'''

#To get all available text on the webpage
'''inner_text = driver.execute_script("return document.documentElement.innerText;")'''
'''print(inner_text)'''

#will highlight particular web element
'''driver.execute_script("arguments[0].style.border = '3px solid red'", best_sellers)'''

#scroll to the bottom
'''driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")'''

#scroll to the top
'''driver.execute_script("window.scrollTo(document.body.scrollHeight, 0)")'''

#scroll till specific element
holiday_gift = driver.find_element(By.XPATH, "//span[contains(text(),'Home Holiday Gifts')]")
driver.execute_script("arguments[0].scrollIntoView(true);", holiday_gift)

