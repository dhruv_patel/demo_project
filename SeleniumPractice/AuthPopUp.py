from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
#driver.get("http://the-internet.herokuapp.com/basic_auth") #Auth popup url
driver.get("http://admin:admin@the-internet.herokuapp.com/basic_auth") #we are logged in using admin cedential
print("Title of this page is: "+driver.title)

