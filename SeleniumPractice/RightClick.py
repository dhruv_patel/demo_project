from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html")
print("Title of this page is: "+driver.title)

'''Right click example'''

right_click_ele = driver.find_element(By.XPATH, "//span[contains(text(),'right click me')]")
act_chain = ActionChains(driver)
act_chain.context_click(right_click_ele).perform()

right_click_option_ele = driver.find_elements(By.CSS_SELECTOR, 'li.context-menu-icon span ')
for ele in right_click_option_ele:
    print(ele.text)
    if ele.text == 'Copy':
        ele.click()
        break