from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://mail.rediff.com/cgi-bin/login.cgi")
print("Title of this page is: "+driver.title)

'''JS Alert Handling'''

driver.find_element(By.NAME, 'proceed').click()

alert = driver.switch_to.alert #will switch from default content to JS alert pop up window
print(alert.text)
time.sleep(3)
alert.accept()
#alert.dismiss()
driver.switch_to.default_content() #will switch back to default content

driver.quit()