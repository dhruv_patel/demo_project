from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("http://classic.crmpro.com/")
print("Title of this page is: "+driver.title)

'''Element Actions Concept'''

username_ele = driver.find_element(By.NAME, 'username')
password_ele = driver.find_element(By.NAME, 'password')
login_ele = driver.find_element(By.XPATH, "//input[@type='submit']")

act_chain = ActionChains(driver)
act_chain.send_keys_to_element(username_ele, 'batchautomation').perform()
act_chain.send_keys_to_element(password_ele, 'Test@1234').perform()
act_chain.click(login_ele).perform()