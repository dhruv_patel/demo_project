from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
import time

'''Chrome'''
#options = webdriver.ChromeOptions()
#options.headless = True
#driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

'''Chrome Incognito without headless'''
options = webdriver.ChromeOptions()
options.headless = False
options.add_argument('--incognito')
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

'''Firefox'''
#options = webdriver.FirefoxOptions()
#options.headless = True
#driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=options)

driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.reddit.com/")
print("Title of this page is: "+driver.title)

'''Headless mode will be used to perform browser actions without opening browser'''