from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.spicejet.com/")
print("Title of this page is: "+driver.title)

'''Move To Element Example'''
act_chains = ActionChains(driver) #ActionChains Object to perform actions

login_ele = driver.find_element(By.ID, 'ctl00_HyperLinkLogin')
act_chains.move_to_element(login_ele).perform()

spiceClubMember_ele = driver.find_element(By.LINK_TEXT, 'SpiceClub Members')
act_chains.move_to_element(spiceClubMember_ele).perform()

memberLogin_ele = driver.find_element(By.LINK_TEXT, 'Member Login').click()

time.sleep(3)
driver.quit()