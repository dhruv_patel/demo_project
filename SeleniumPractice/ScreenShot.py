from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time

options = webdriver.ChromeOptions()
options.headless = True
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
driver.implicitly_wait(10)
driver.maximize_window()
driver.get("https://www.amazon.ca/")
print("Title of this page is: "+driver.title)

'''visible area screenshot'''
driver.get_screenshot_as_file("amazon_screenshot_1.png")

'''full screenshot (only works in headless mode)'''
S = lambda X: driver.execute_script('return document.body.parentNode.scroll'+X)
driver.set_window_size(S('Width'), S('Height'))
driver.find_element_by_tag_name('body').screenshot('amazon_full_screenshot_1.png')

driver.quit()