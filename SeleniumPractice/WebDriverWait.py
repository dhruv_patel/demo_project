from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from webdriver_manager.chrome import ChromeDriverManager
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://app.hubspot.com/login")
print("Title of this page is: "+driver.title)

#this will work as explicit wait
#can be used for any particular DOM element to wait that cannot be handled by implicit wait such as title, url, etc
wait = WebDriverWait(driver, 10)
email_id = wait.until(ec.presence_of_element_located((By.ID, 'username')))
email_id.send_keys('test@gmail.com')



