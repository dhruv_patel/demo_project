from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from webdriver_manager.chrome import ChromeDriverManager
import time


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://www.freshworks.com/")
print("Title of this page is: "+driver.title)

wait = WebDriverWait(driver, 10)
footer_links = wait.until(ec.presence_of_all_elements_located((By.CSS_SELECTOR, 'ul.footer-nav li')))
print(len(footer_links))

for ele in footer_links:
    print(ele.text)

#we can also wait until frame is available and switch to it
'''wait.until(ec.frame_to_be_available_and_switch_to_it(By.ID, 'test'))'''

#we can also use for element is selected or no
'''wait.until(ec.element_located_to_be_selected(By.ID, 'test'))'''
