'''first we ned to install xlrd using: pip install xlrd but the new versions does not support anything except xls file'''
'''to overcome this issue install previous version by using: pip install xlrd==1.2.0'''
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from webdriver_manager.chrome import ChromeDriverManager
import time
import xlrd


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://www.orangehrm.com/orangehrm-30-day-trial/")
print("Title of this page is: "+driver.title)

url = driver.find_element(By.ID, 'Form_submitForm_subdomain')
first_name = driver.find_element(By.ID, 'Form_submitForm_FirstName')
last_name = driver.find_element(By.ID, 'Form_submitForm_LastName')
email = driver.find_element(By.ID, 'Form_submitForm_Email')
job_title = driver.find_element(By.ID, 'Form_submitForm_JobTitle')
no_of_emp = driver.find_element(By.ID, 'Form_submitForm_NoOfEmployees')
company = driver.find_element(By.ID, 'Form_submitForm_CompanyName')
industry = driver.find_element(By.ID, 'Form_submitForm_Industry')
phone = driver.find_element(By.ID, 'Form_submitForm_Contact')
country = driver.find_element(By.ID, 'Form_submitForm_Country')



workbook = xlrd.open_workbook("DataFile1.xlsx")
sheet = workbook.sheet_by_name("registration")

'''total number of rows and column'''
rowCount = sheet.nrows
columnCount = sheet.ncols
print(rowCount)
print(columnCount)

'''to get data'''
for curr_row in range(1, rowCount):
    url_value = sheet.cell_value(curr_row, 0)
    first_name_value = sheet.cell_value(curr_row, 1)
    last_name_value = sheet.cell_value(curr_row, 2)
    email_value = sheet.cell_value(curr_row, 3)
    job_title_value = sheet.cell_value(curr_row, 4)
    company_value = sheet.cell_value(curr_row, 5)
    phone_value = sheet.cell_value(curr_row, 6)
    no_of_emp_value = sheet.cell_value(curr_row, 7)
    industry_value = sheet.cell_value(curr_row, 8)
    country_value = sheet.cell_value(curr_row, 9)

    print(url, first_name, last_name, email, job_title, company, phone, no_of_emp, industry, country)

    #entering values from excel sheet to browser
    url.clear()
    url.send_keys(url_value)
    first_name.clear()
    first_name.send_keys(first_name_value)
    last_name.clear()
    last_name.send_keys(last_name_value)
    email.clear()
    email.send_keys(email_value)
    job_title.clear()
    job_title.send_keys(job_title_value)
    company.clear()
    company.send_keys(company_value)
    #no_of_emp.clear()
    no_of_emp.send_keys(no_of_emp_value)
    #industry.clear()
    industry.send_keys(industry_value)
    #country.clear()
    country.send_keys(country_value)
    time.sleep(3)