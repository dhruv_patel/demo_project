from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
import time

browserName = "chrome"

#to use different browser
if browserName == "chrome":
    driver = webdriver.Chrome(ChromeDriverManager().install())
elif browserName == "firefox":
    driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
elif browserName == "safari":
    driver = webdriver.Safari()
else:
    print("Please select correct browser name: " +browserName)
    raise Exception("driver is not found")  #to throw an exception

driver.implicitly_wait(5)
driver.maximize_window()

driver.get("https://www.facebook.com/")

emailField = driver.find_element(By.NAME, 'email').send_keys("abcd@gmail.com")

passField = driver.find_element(By.ID, 'pass').send_keys("123abc")

loginButton = driver.find_element(By.NAME, 'login').click()

time.sleep(5)
driver.quit()
