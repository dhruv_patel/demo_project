import pytest

@pytest.mark.login #used for grouping
def test_m1():
    a = 3
    b = 4
    assert a+1 == b, "test passed"
    assert a == b, "test failed as a is equal to b"

def test_m2():
    name = "Dhruv"
    assert name.upper() == "DHRUV"

@pytest.mark.login
def test_m3():
    assert True

def test_m4():
    assert "Dhruv" == "DhruV"

def test_login():
    assert "login" == "login"
