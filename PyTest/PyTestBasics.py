import pytest

'''PyTest will only execute files that starts with test or ends with test'''
'''to install: pip install pytest'''
'''to run through terminal just type py.test : it will run all pytest files'''
'''to run single pytest file we need go through that library and run it using path for an example: py.test PyTest/test_demo2.py'''
'''to run particular test using keyword, example: py.test -k login -v (it will run only testcases contains keyword "login")'''
'''for grouping we can use @pytest.mark.(whatever grouping name you choose)'''
'''to run grouping use py.test -m (whatever grouping name you created)'''

'''use this command to install parallel testing mode : pip install pytest-xdist'''
'''to run parallel mode using 5 browser window example: py.test PyTest/test_webpage_login.py -n 5'''
'''to generate html report, install: pip install pytest-html'''

'''to run, example: pytest PyTest/test_google_test.py -v -s -n2 --html=google_test_report.html'''
# -v will give us more than one result
# -s will display the print statements for that method
# -n2 is for parallel execution and 2 stats it will open two browser at same time
#------------------------------------------------